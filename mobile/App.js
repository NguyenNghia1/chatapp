import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput } from 'react-native';
import io from "socket.io-client";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chatMessage: '',
      chatMessages: [],
      disconnect: ''
    }
  }

  componentDidMount() {
    //Truy cap bat cu dau?
    this.socket = io("http://192.168.0.116:3000")
    this.socket.on("chat message", msg => {
      //[...this.state.chatMessages, msg] add msg to chatMessages and return new array 
      this.setState({chatMessages: [...this.state.chatMessages, msg]})
    })
    this.socket.on("disconnect", msg => {
      this.setState({disconnect: msg})
    })
  }

  submitEditing() {
    this.socket.emit("chat message", this.state.chatMessage);
    this.setState({ chatMessage: "" });
  }

  render() {
    const chatMessages = this.state.chatMessages.map(chatMessage => (
      <Text key={chatMessage}>{chatMessage}</Text>
    ));
    return (
      <View style={styles.container}>
      <TextInput 
        style={styles.input}
        onChangeText={(text) => this.setState({chatMessage: text})}
        onSubmitEditing={() => this.submitEditing()}
      />
      {chatMessages}
      <Text>{this.state.disconnect}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex:1,
    backgroundColor: 'white',
  },
  input: {
    paddingLeft: 20,
    height: 50,
    fontSize: 25,
    backgroundColor: 'white',
    borderColor: '#1abc9c',
    borderWidth: 1,
    marginBottom: 20,
    color: '#34495e'
}
})
